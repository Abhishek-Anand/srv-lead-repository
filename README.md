# SLR - SRV Leads Repository

## Intro

A centralized leads data repository, to store, extract and analyse quality leads for a digital marketing strategy.